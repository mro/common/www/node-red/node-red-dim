"use strict";

const
  { invoke } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimConf').ConfNode} ConfNode
 * @typedef {import('dim').DisService} DisService
 *
 * @typedef {nodeRedDim.DimServerDef & Pick<ExtraProps, "value">} DisValueDef
 * @typedef {nodeRedDim.DimServerNode & ExtraProps} DisValueNode
 * @typedef {{
 *  value: any,
 *  _convert(value: any): any
 *  _unconvert(value: any): any
 * }} ExtraProps
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM Value Server Node
   * @param {DisValueDef} c
   * @this {DisValueNode}
   */
  function DisVal(c) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    this.serviceDef = c.serviceDef;
    this.value = c.value;
    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);
      node.disNode = node.cnf.getDisNode();
      if (node.disNode) {
        node.cnf.ref();
        const srv = node.disNode.addService(node.serviceName, node.serviceDef,
          node.value);
        const up = (/** @type {any} */val) => {
          const msg = { _msgid: RED.util.generateId(), payload: val };
          try {
            msg.payload = (val !== undefined) ? node._unconvert(val) :
              undefined;
            node.send(msg);
          }
          catch (err) {
            node.error(err.toString(), msg);
          }
        };
        node._convert = node._unconvert = (value) => value; /* no conversion */

        if (!srv) {
          invoke(node, '_removeConnStatus');
          if (invoke(node.disNode, 'isService', node.serviceName)) {
            node.error(RED._("disValue.error.dup-service",
              { service: node.serviceName }));
          }
          else {
            node.error(RED._("disValue.error.service-reg-failed",
              { service: node.serviceName }));
          }
          node.disNode = null;
        }
        else {
          srv.on('update', up);

          node.on('input', function(msg, send, done) {
            if (msg.hasOwnProperty("payload")) {
              try {
                srv.setValue(node._convert(msg.payload));
              }
              catch (err) {
                node.error(err.toString(), msg);
              }
            }
            done();
          });
        }

        node.on('close', function(/** @type {() => void} */ done) {
          invoke(srv, 'removeListener', 'update', up);
          invoke(node.disNode, 'removeService', node.serviceName);
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.disNode = null;
          done();
        });
      }
    }
    else {
      node.error(RED._("node-red-dim/dimConf:dimConf.error.config-missing"));
    }

  }
  RED.nodes.registerType("dim-value-server", DisVal);

  /**
   * @brief constructor function of DIM JSON Client Node
   * @param {DisValueDef} c
   * @this {DisValueNode}
   */
  function DisJson(c) {
    c.serviceDef = "C";
    DisVal.call(this, c);
    this._convert = (/** @type {any} */ value) => JSON.stringify(value);
    this._unconvert = (/** @type {string} */ value) => JSON.parse(value);
  }
  RED.nodes.registerType("dim-json-server", DisJson);
};
