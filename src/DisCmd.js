"use strict";

const
  { invoke } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimConf').ConfNode} ConfNode
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM CMD Server Node
   * @param {nodeRedDim.DimServerDef} c
   * @this {nodeRedDim.DimServerNode}
   */
  function DimCmdServer(c) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    this.serviceDef = c.serviceDef;
    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);
      node.disNode = node.cnf.getDisNode();
      if (node.disNode) {
        node.cnf.ref();
        const cmd = node.disNode.addCmd(node.serviceName, node.serviceDef,
          (req) => {
            var msg = { _msgid: RED.util.generateId(), payload: req.value };
            node.send(msg);
          });

        if (!cmd) {
          invoke(node, '_removeConnStatus');
          if (invoke(node.disNode, 'isService', node.serviceName)) {
            node.error(RED._("disCmd.error.dup-service",
              { service: node.serviceName }));
          }
          else {
            node.error(RED._("disCmd.error.service-reg-failed",
              { service: node.serviceName }));
          }
          node.disNode = null;
        }

        node.on('close', function(/** @type {() => void} */ done) {
          invoke(node.disNode, 'removeCmd', node.serviceName);
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.disNode = null;
          done();
        });
      }
    }
    else {
      node.error(RED._("node-red-dim/dimConf:dimConf.error.config-missing"));
    }
  }
  RED.nodes.registerType("dim-cmd-server", DimCmdServer);
};
