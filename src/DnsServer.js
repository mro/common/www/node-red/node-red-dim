"use strict";

const
  { DnsServer, ServiceInfo } = require('dim');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('@node-red/registry').Node} Node
 * @typedef {import('@node-red/registry').NodeDef} NodeDef
 *
 * @typedef {{ port: number }} DnsNodeDef
 * @typedef {{ _port: number, _dnsServer: DnsServer }} DnsNode
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM Name Server Node
   * @param {NodeDef & DnsNodeDef} c
   * @this {Node & DnsNode}
   */
  function DnsSrv(c) {
    RED.nodes.createNode(this, c);
    this._port = c.port;
    const node = this;

    node._dnsServer = new DnsServer('localhost', node._port);

    node._dnsServer.listen().then(() => {
      node.emit('listening');
      node.status({ fill: "green", shape: "dot",
        text: "node-red-dim/dnsServer:dnsServer.status.ready" });
      node._dnsServer.on('update', (srvInfo) => {
        if (srvInfo instanceof ServiceInfo) {
          /** @type {{ _msgid: string, payload: string, node?: string }} */
          var msg = {
            _msgid: RED.util.generateId(),
            payload: (srvInfo.removed ? '- ' : '+ ') + srvInfo.name
          };
          if (srvInfo.node) {
            msg.node = srvInfo.node.task + '@' + srvInfo.node.name;
          }
          node.send(msg);
        }
      });
    });

    node.on('close', (/** @type {() => void} */ done) => {
      node._dnsServer.close();
      node.status({ fill: "red", shape: "dot",
        text: "node-red-dim/dnsServer:dnsServer.status.stopped" });
      done();
    });
  }
  RED.nodes.registerType("dim-dns-server", DnsSrv);
};
