import { Node, NodeDef } from '@node-red/registry';
import { DisNode, DnsClient, ServiceInfo } from 'dim';
import { ConfNode } from './DimConf';

export = nodeRedDim;
export as namespace nodeRedDim;

declare namespace nodeRedDim {
  interface DimNodeDef extends NodeDef {
    cnf: string; /* node id */
    serviceName: string;
  }
  type DimClientDef = DimNodeDef & { timeout: number };
  type DimServerDef = DimNodeDef & { serviceDef: string };

  interface DimNode extends Node {
    cnf: ConfNode;
    serviceName: string;

    _removeConnStatus?: (() => void) | null;
  }

  type DimClientNode = DimNode & {
    timeout: number;
    service: string | ServiceInfo;
    dnsClient: DnsClient | null;
  };
  type DimServerNode = DimNode & {
    serviceDef: string;
    disNode: DisNode | null;
  };
}
