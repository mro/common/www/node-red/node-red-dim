"use strict";

const
  { DicValue, packets } = require('dim'),
  { invoke, get } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimConf').ConfNode} ConfNode
 *
 * @typedef {nodeRedDim.DimClientDef &
 *  Pick<ExtraProps, "monitored" | "scheduled" | "stamped">} DicValueDef
 * @typedef {nodeRedDim.DimClientNode & ExtraProps} DicValueNode
 * @typedef {{
 *  monitored: boolean,
 *  scheduled: boolean,
 *  stamped: boolean,
 *  _options: {timeout: number, stamped: boolean, type?: packets.DicReq.Type},
 *  _convert(value: any): any,
 *  _dicValue: DicValue<any>
 * }} ExtraProps
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM Value Client Node
   * @param {DicValueDef} c
   * @this {DicValueNode}
   */
  /* eslint-disable max-statements */
  function DicVal(c) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    this.monitored = c.monitored;
    this.scheduled = c.scheduled;
    this.timeout = c.timeout; /* ms */
    this.stamped = c.stamped;

    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);
      node.service = node.serviceName;
      node.dnsClient = node.cnf.getDnsClient();
      if (node.dnsClient) {
        node.cnf.ref();

        node._options = {
          timeout: 0,
          stamped: node.stamped,
          type: undefined
        };

        if (node.monitored) {
          node._options.type = packets.DicReq.Type.MONITORED;
          if (node.scheduled) {
            node._options.timeout = node.timeout;
          }
        }
        else if (node.scheduled) {
          node._options.type = packets.DicReq.Type.TIMED_ONLY;
          node._options.timeout = node.timeout;
        }

        node._convert = (value) => value; /* no conversion */

        /** @type {function(any): void} */
        const sendMsg = (val) => {
          /** @type {{ _msgid: string, payload?: any, timestamp?: number? }} */
          const msg = { _msgid: RED.util.generateId() };
          try {
            msg.payload = (val !== undefined) ? node._convert(val) : undefined;

            if (node._dicValue && node.stamped) {
              msg.timestamp = get(node._dicValue, 'timestamp');
            }
            node.send(msg);
          }
          catch (err) {
            msg.payload = val;
            node.error(err.toString(), msg);
          }
        };

        if (node._options.type) {
          node._dicValue = new DicValue(node.service, node._options,
            node.dnsClient);
          node._dicValue.on('value', sendMsg);

          /* check on the first value */
          (async function() {
            await node._dicValue.promise();
          }())
          .catch(() => {
            node._dicValue.removeListener('value', sendMsg);
            node.error(RED._("dicValue.error.service-released",
              { error: node.serviceName }));
          });
        }
        else {
          node.dnsClient.query(node.serviceName)
          .then(
            (srv) => { node.service = srv; },
            (err) => { node.error(err); });
        }

        node.on('input', function(msg, send, done) {
          DicValue.get(node.service, node.dnsClient)
          .then((val) => { sendMsg(val); done(); })
          .catch(done);
        });

        node.on('close', (/** @type {() => void} */ done) => {
          if (node._dicValue) {
            node._dicValue.removeListener('value', sendMsg);
            node._dicValue.release();
          }
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.dnsClient = null;
          done();
        });

        RED.httpAdmin.post("/" + this.type + "/:id", function(req, res) {
          var node = RED.nodes.getNode(req.params.id);
          if (node !== null) {
            try {
              node.receive(); /* trigger an 'input' event */
              res.sendStatus(200);
            }
            catch (err) {
              res.sendStatus(500);
              node.error(RED._("dicValue.error.fetch-failed-with-err",
                { error: err.toString() }));
            }
          }
          else {
            res.sendStatus(404);
          }
        });
      }
    }
    else {
      node.error(RED._("node-red-dim/dimConf:dimConf.error.config-missing"));
    }

  }
  RED.nodes.registerType("dim-value-client", DicVal);

  /**
   * @brief constructor function of DIM JSON Client Node
   * @param {DicValueDef} c
   * @this {DicValueNode}
   */
  function DicJson(c) {
    DicVal.call(this, c);
    this._convert = (/** @type {string} */ value) => JSON.parse(value);
  }
  RED.nodes.registerType("dim-json-client", DicJson);
};
