const
  DimConf = require('./DimConf'),
  DnsServer = require('./DnsServer'),
  DicCmd = require('./DicCmd'),
  DisCmd = require('./DisCmd'),
  DicRpc = require('./DicRpc'),
  DisRpc = require('./DisRpc'),
  DicValue = require('./DicValue'),
  DisValue = require('./DisValue'),
  DicJson = require('./DicValue'),
  DisJson = require('./DisValue');

module.exports = {
  DimConf, DnsServer, DicCmd, DisCmd, DicRpc, DisRpc, DicValue, DisValue,
  DicJson, DisJson };
