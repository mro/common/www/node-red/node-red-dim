"use strict";

const
  { DicRpc } = require('dim'),
  { invoke } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimConf').ConfNode} ConfNode
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM RPC Client Node
   * @param {nodeRedDim.DimClientDef} c
   * @this {nodeRedDim.DimClientNode}
   */
  function DimRpcReq(c) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    this.timeout = c.timeout; /* ms */

    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);
      node.service = node.serviceName;
      node.dnsClient = node.cnf.getDnsClient();
      if (node.dnsClient) {
        node.cnf.ref();
        node.dnsClient.query(node.serviceName)
        .then((srv) => {
          node.service = srv;
        }, (err) => {
          node.error(err);
        });

        node.on('input', function(msg, send, done) {
          if (msg.hasOwnProperty("payload")) {
            DicRpc.invoke(node.service, msg.payload, node.dnsClient,
              node.timeout)
            .then((value) => {
              msg.payload = value;
              send(msg);
              done();
            }, done);
          }
          else {
            done();
          }
        });

        node.on('close', (/** @type {() => void} */ done) => {
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.dnsClient = null;
          done();
        });
      }
    }
    else {
      node.error(RED._("node-red-dim/dimConf:dimConf.error.config-missing"));
    }
  }
  RED.nodes.registerType("dim-rpc-client", DimRpcReq);
};
