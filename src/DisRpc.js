"use strict";

const
  TIMEOUT = require('dim').DicRpc.TIMEOUT,
  { makeDeferred, timeout } = require('prom'),
  { invoke, get, defaultTo } = require('lodash');

/**
 * @typedef {import('@node-red/registry').NodeAPI} Red
 * @typedef {import('./DimConf').ConfNode} ConfNode
 */

/**
 * @param {Red} RED
 */
module.exports = function(RED) {

  /**
   * @brief constructor function of DIM RPC Response Node
   * @param {nodeRedDim.DimServerDef} c
   * @this {nodeRedDim.DimServerNode}
   */
  function DimRpcRes(c) {
    RED.nodes.createNode(this, c);
    const node = this;

    node.on('input', function(msg, send, done) {
      if (msg.hasOwnProperty("payload")) {
        // @ts-ignore: 'res' checked in the first condition
        if ((get(msg, 'res.promise') instanceof Promise) && msg.res.isPending) {
          try {
            // @ts-ignore: 'res' checked above
            msg.res.resolve(msg.payload);
            node.debug(RED._("disRpc.success.request-served") + ": " +
              msg.payload);
            done();
          }
          catch (err) {
            done(err);
          }
        }
        else {
          done(new Error(RED._("disRpc.error.request-failed")));
        }
      }
      else {
        done();
      }
    });
  }
  RED.nodes.registerType("dim-rpc-response", DimRpcRes);

  /**
   * @brief constructor function of DIM RPC In Node
   * @param {nodeRedDim.DimServerDef} c
   * @this {nodeRedDim.DimServerNode}
   */
  function DimRpcIn(c) {
    RED.nodes.createNode(this, c);
    this.cnf = /** @type {ConfNode} */(RED.nodes.getNode(c.cnf));
    this.serviceName = c.serviceName;
    this.serviceDef = c.serviceDef;
    const node = this;

    if (node.cnf) {
      node._removeConnStatus = node.cnf.addConnStatus(node);

      node.disNode = node.cnf.getDisNode();
      if (node.disNode) {
        node.cnf.ref();
        const rpc = node.disNode.addRpc(node.serviceName, node.serviceDef,
          (req) => {
            const d = makeDeferred();
            var msg = { _msgid: RED.util.generateId(), payload: req.value,
              res: d };
            const reply = timeout(d.promise, defaultTo(req.timeout, TIMEOUT));
            node.send(msg);
            return reply;
          });

        if (!rpc) {
          invoke(node, '_removeConnStatus');
          if (invoke(node.disNode, 'isService', node.serviceName +
            '/RpcIn') || invoke(node.disNode, 'isService', node.serviceName +
            '/RpcOut')) {
            node.error(RED._("disRpc.error.dup-service",
              { service: node.serviceName }));
          }
          else {
            node.error(RED._("disRpc.error.service-reg-failed",
              { service: node.serviceName }));
          }
          node.disNode = null;
        }

        node.on('close', function(/** @type {() => void} */ done) {
          invoke(node.disNode, 'removeRpc', node.serviceName);
          if (node._removeConnStatus) {
            node._removeConnStatus();
            delete node._removeConnStatus;
          }
          invoke(node.cnf, 'unref');
          node.disNode = null;
          done();
        });
      }
    }
    else {
      node.error(RED._("node-red-dim/dimConf:dimConf.error.config-missing"));
    }
  }
  RED.nodes.registerType("dim-rpc in", DimRpcIn);
};
