const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { invoke } = require('lodash'),
  { waitEvent } = require('dim/test/utils'),
  { waitForValue } = require('./utils'),

  { DnsServer, DisNode, DnsClient } = require('dim'),
  { DicCmd, DimConf } = require('../src');

describe('DicCmd Node', function() {
  var env = {};

  beforeEach(async function() {
    env.dnsServer = new DnsServer('localhost', 2505);
    env.dis = new DisNode();
    env.dis.addCmd('cmd/test', 'C', (req) => {
      env.ret.push(req.value);
      env.dis.emit('cmdReceived');
    });
    env.ret = [];
    await env.dnsServer.listen();
    const dnsClient = new DnsClient('localhost', 2505);
    await dnsClient.register(env.dis);
    dnsClient.unref();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'removeAllListeners', 'cmdReceived');
    invoke(env.dis, 'close');
    invoke(env.dnsServer, 'close');
    env = {};
  });

  it('is loaded correctly', function(done) {
    const flow = [
      { id: "dicCmd", type: "dim-cmd-client", name: "DIM Cmd Client",
        cnf: "conf", serviceName: "cmd/test", timeout: "1000", wires: [] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DicCmd, DimConf ], flow, async function() {
      const dicCmdNode = helper.getNode("dicCmd");
      try {
        expect(dicCmdNode).to.have.property('name', 'DIM Cmd Client');
        expect(dicCmdNode.cnf).to.have.property('_host', 'localhost');
        expect(dicCmdNode.cnf).to.have.property('_port', '2505');
        expect(dicCmdNode.cnf).to.have.property('_retry', '1000');
        expect(dicCmdNode).to.have.property('serviceName', 'cmd/test');
        expect(dicCmdNode).to.have.property('timeout', '1000');
        expect(dicCmdNode.wires).to.be.an('array').that.is.empty();

        await waitEvent(dicCmdNode.cnf.getDnsClient(), 'connect');
        expect(dicCmdNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dimConf:dimConf.status.connected" } ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can send a command', function(done) {
    const flow = [
      { id: "dicCmd", type: "dim-cmd-client", name: "DIM Cmd",
        cnf: "conf", serviceName: "cmd/test", timeout: "1000" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DicCmd, DimConf ], flow, async function() {
      const dicCmdNode = helper.getNode("dicCmd");
      try {
        const prom = waitForValue(env.dis, 'cmdReceived', () => {
          return env.ret[env.ret.length - 1];
        }, 'A1B2C3');
        dicCmdNode.receive({ payload: "A1B2C3" });
        await prom;

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error if the Dns Configuration is not set', function(done) {
    const flow = [
      { id: "dicCmd", type: "dim-cmd-client", name: "DIM Cmd",
        cnf: "conf", serviceName: "cmd/test", timeout: "1000" }
    ];

    helper.load(DicCmd, flow, function() {
      const dicCmdNode = helper.getNode("dicCmd");
      try {
        expect(dicCmdNode.error.lastCall.args).to.be.deep
        .equal([ 'node-red-dim/dimConf:dimConf.error.config-missing' ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error if the Dns Configuration is wrong', function(done) {
    const flow = [
      { id: "dicCmd", type: "dim-cmd-client", name: "DIM Cmd",
        cnf: "conf", serviceName: "cmd/test", timeout: "1000" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "1234",
        retry: "1000" }
    ];

    helper.load([ DicCmd, DimConf ], flow, function() {
      const dicCmdNode = helper.getNode("dicCmd");

      dicCmdNode.once('call:error', function(call) {
        try {
          expect(call.args.toString())
          .to.be.deep.equal('Error: failed to connect');

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });
  });

  it('raises an error if service doesn\'t exist', function(done) {
    const srvName = "test/cmd";
    const flow = [
      { id: "dicCmd", type: "dim-cmd-client", name: "DIM Cmd",
        cnf: "conf", serviceName: srvName, timeout: "1000" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DicCmd, DimConf ], flow, async function() {
      const dicCmdNode = helper.getNode("dicCmd");

      dicCmdNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: service not found ' + srvName);

          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(dicCmdNode.cnf.getDnsClient(), 'connect');
        expect(dicCmdNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dimConf:dimConf.status.connected" } ]);
        dicCmdNode.receive({ payload: "A1B2C3" });
      }
      catch (err) {
        done(err);
      }
    });
  });
});
