/* eslint-disable max-lines*/
const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { invoke } = require('lodash'),
  { waitEvent } = require('dim/test/utils'),

  { DnsServer, DicRpc, DisNode, DnsClient, NodeInfo } = require('dim'),
  { DisRpc, DimConf } = require('../src');

describe('DisRpc Nodes', function() {

  describe('DimRpcIn Node', function() {
    var env = {};

    beforeEach(async function() {
      env.dns = new DnsServer('localhost', 2505);
      await env.dns.listen();
    });

    afterEach(async function() {
      await helper.unload();
      invoke(env.dis, 'close');
      invoke(env.dns, 'close');
      env = {};
    });

    it('is loaded correctly', function(done) {
      const flow = [
        { id: "dimRpcIn", type: "dim-rpc in", name: "DIM Rpc In",
          cnf: "conf", serviceName: "rpc/test", serviceDef: "I,C",
          wires: [ [ "help" ] ] },
        { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
          retry: "1000" }
      ];

      helper.load([ DisRpc, DimConf ], flow, async function() {
        const dimRpcInNode = helper.getNode("dimRpcIn");
        try {
          expect(dimRpcInNode).to.have.property('name', 'DIM Rpc In');
          expect(dimRpcInNode.cnf).to.have.property('_host', 'localhost');
          expect(dimRpcInNode.cnf).to.have.property('_port', '2505');
          expect(dimRpcInNode.cnf).to.have.property('_retry', '1000');
          expect(dimRpcInNode).to.have.property('serviceName', 'rpc/test');
          expect(dimRpcInNode).to.have.property('serviceDef', 'I,C');
          expect(dimRpcInNode.wires).to.be.an('array')
          .that.deep.equal([ [ "help" ] ]);

          await waitEvent(dimRpcInNode.cnf.getDnsClient(), 'connect');
          expect(dimRpcInNode.status.lastCall.args).to.be.deep.equal(
            [ { fill: "green", shape: "dot",
              text: "node-red-dim/dimConf:dimConf.status.connected" } ]);

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('can receive a RPC request', function(done) {
      const flow = [
        { id: "dimRpcIn", type: "dim-rpc in", name: "DIM Rpc In",
          cnf: "conf", serviceName: "rpc/test", serviceDef: "C,I",
          wires: [ [ "help" ] ] },
        { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
          retry: "1000" },
        { id: "help", type: "helper" }
      ];

      helper.load([ DisRpc, DimConf ], flow, async function() {
        const dimRpcInNode = helper.getNode("dimRpcIn");
        const helperNode = helper.getNode("help");

        helperNode.once('input', function(msg) {
          try {
            expect(dimRpcInNode.send.callCount).to.be.equal(1);

            expect(msg.payload).to.be.a('string').that.deep.equal('123');

            done();
          }
          catch (err) {
            done(err);
          }
        });

        try {
          await waitEvent(dimRpcInNode.cnf.getDnsClient(), 'connect');
          expect(dimRpcInNode.status.lastCall.args).to.be.deep.equal(
            [ { fill: "green", shape: "dot",
              text: "node-red-dim/dimConf:dimConf.status.connected" } ]);

          DicRpc.invoke('rpc/test', '123', env.dns.url());
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('raises an error if the Dns Configuration is not set', function(done) {
      const flow = [
        { id: "dimRpcIn", type: "dim-rpc in", name: "DIM Rpc In",
          cnf: "conf", serviceName: "rpc/test", serviceDef: "I,C" }
      ];

      helper.load(DisRpc, flow, function() {
        const dimRpcInNode = helper.getNode("dimRpcIn");
        try {
          expect(dimRpcInNode.error.lastCall.args).to.be.deep
          .equal([ 'node-red-dim/dimConf:dimConf.error.config-missing' ]);

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('raises an error in case of duplicate taskNames', function(done) {
      const flow = [
        { id: "dimRpcIn", type: "dim-rpc in", name: "DIM Rpc In",
          cnf: "conf", serviceName: "test/rpc1", serviceDef: "C,I" },
        { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
          retry: "1000" }
      ];
      env.dis = new DisNode();
      env.dis.addRpc('test/rpc2', 'I,I', (req) => req.value);
      let dnsClient = new DnsClient('localhost', 2505);
      dnsClient.register(env.dis).then(() => {
        dnsClient.unref();
        dnsClient = null;
      });


      helper.load([ DisRpc, DimConf ], flow, function() {
        const dimRpcInNode = helper.getNode("dimRpcIn");
        const taskName = dimRpcInNode.disNode.info.task;

        dimRpcInNode.cnf.once('call:error', function(call) {
          try {
            expect(call.args).to.be.deep.equal(
              [ 'dimConf.error.dup-taskName' ]);

            done();
          }
          catch (err) {
            done(err);
          }
        });

        try {
          expect(env.dis.info.task).to.be.deep.equal(taskName);
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('raises an error in case of duplicate services on different' +
    ' DIM Service Providers', function(done) {
      const flow = [
        { id: "dimRpcIn", type: "dim-rpc in", name: "DIM Rpc In",
          cnf: "conf", serviceName: "test/rpc", serviceDef: "C,I" },
        { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
          retry: "1000" }
      ];

      env.dis = new DisNode(NodeInfo.local(0, 0, 'taskName2'));
      env.dis.addRpc('test/rpc', 'I,I', (req) => req.value);
      let dnsClient = new DnsClient('localhost', 2505);
      dnsClient.register(env.dis).then(() => {
        dnsClient.unref();
        dnsClient = null;

        helper.load([ DisRpc, DimConf ], flow, function() {
          const dimRpcInNode = helper.getNode("dimRpcIn");

          dimRpcInNode.cnf.once('call:error', function(call) {
            try {
              expect(call.args).to.be.deep.equal(
                [ 'dimConf.error.dup-service' ]);

              done();
            }
            catch (err) {
              done(err);
            }
          });
        });
      });
    });

    it('raises an error in case of duplicate services on the same' +
  ' DIM Service Provider', function(done) {
      const flow = [
        /* config node */
        { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
          retry: "1000" },
        /* first node */
        { id: "dimRpcIn1", type: "dim-rpc in", name: "DIM Rpc In 1",
          cnf: "conf", serviceName: "test/rpc", serviceDef: "C,I" },
        /* second node */
        { id: "dimRpcIn2", type: "dim-rpc in", name: "DIM Rpc In 2",
          cnf: "conf", serviceName: "test/rpc", serviceDef: "C,I" }
      ];

      helper.load([ DisRpc, DimConf ], flow, function() {
        const disRpcInNode1 = helper.getNode("dimRpcIn1");
        const disRpcInNode2 = helper.getNode("dimRpcIn2");

        var f1 = function(call) {
          disRpcInNode2.removeListener('call:error', f2);
          try {
            expect(call.args).to.be.deep.equal([ 'disRpc.error.dup-service' ]);

            done();
          }
          catch (err) {
            done(err);
          }
        };

        var f2 = function(call) {
          disRpcInNode1.removeListener('call:error', f1);
          try {
            expect(call.args).to.be.deep.equal([ 'disRpc.error.dup-service' ]);

            done();
          }
          catch (err) {
            done(err);
          }
        };

        disRpcInNode1.once('call:error', f1);
        disRpcInNode2.once('call:error', f2);
      });
    });

    it('removes the RPC service when distroyed', function(done) {
      const flow = [
        { id: "dimRpcIn", type: "dim-rpc in", name: "DIM Rpc In",
          cnf: "conf", serviceName: "rpc/test", serviceDef: "C,I" },
        { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
          retry: "1000" }
      ];

      helper.load([ DisRpc, DimConf ], flow, async function() {
        const dimRpcResNode = helper.getNode("dimRpcIn");
        env.dis = dimRpcResNode.disNode;

        try {
          await waitEvent(dimRpcResNode.cnf.getDnsClient(), 'connect');

          expect(env.dis.isService('rpc/test/RpcIn')).to.be.true();
          expect(env.dis.isService('rpc/test/RpcOut')).to.be.true();
        }
        catch (err) {
          done(err);
        }

        helper.clearFlows() /* destroy the node */
        .then(() => {
          expect(env.dis.isService('rpc/test/RpcIn')).to.be.false();
          expect(env.dis.isService('rpc/test/RpcOut')).to.be.false();
          done();
        })
        .catch(done);
      });
    });
  });

  describe('DimRpcRes Node', function() {
    var env = {};

    beforeEach(async function() {
      env.dns = new DnsServer('localhost', 2505);
      await env.dns.listen();
    });

    afterEach(async function() {
      await helper.unload();
      invoke(env.dns, 'close');
      env = {};
    });

    it('is loaded correctly', function(done) {
      const flow = [
        { id: "dimRpcRes", type: "dim-rpc-response", name: "DIM Rpc Res",
          wires: [] }
      ];

      helper.load(DisRpc, flow, function() {
        const dimRpcResNode = helper.getNode("dimRpcRes");

        try {
          expect(dimRpcResNode).to.have.property('name', 'DIM Rpc Res');
          expect(dimRpcResNode.wires).to.be.an('array').that.is.empty();

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('can serve a RPC request', function(done) {
      const msg = { payload: '1234' };
      const flow = [
        { id: "dimRpcIn", type: "dim-rpc in", name: "DIM Rpc In",
          cnf: "conf", serviceName: "rpc/test", serviceDef: "C,C",
          wires: [ [ "dimRpcRes" ] ] },
        { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
          retry: "1000" },
        { id: "dimRpcRes", type: "dim-rpc-response" }
      ];

      helper.load([ DisRpc, DimConf ], flow, async function() {
        const dimRpcInNode = helper.getNode('dimRpcIn');
        const dimRpcResNode = helper.getNode("dimRpcRes");

        dimRpcResNode.once('call:debug', function(call) {
          try {
            expect(call.args).to.be.deep.equal(
              [ 'disRpc.success.request-served: ' + msg.payload ]);

            done();
          }
          catch (err) {
            done(err);
          }
        });

        try {
          await waitEvent(dimRpcInNode.cnf.getDnsClient(), 'connect');
          DicRpc.invoke('rpc/test', msg.payload, env.dns.url());
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('raises an error if the input message doesn\'t come from' +
      ' a DimRpcIn node', function(done) {
      const flow = [ { id: "dimRpcRes", type: "dim-rpc-response" } ];

      helper.load(DisRpc, flow, function() {
        const dimRpcResNode = helper.getNode("dimRpcRes");

        dimRpcResNode.once('call:error', function(call) {
          try {
            expect(call.firstArg.toString())
            .to.be.deep.equal('Error: disRpc.error.request-failed');

            done();
          }
          catch (err) {
            done(err);
          }
        });

        const msg = { payload: '1234' };
        dimRpcResNode.receive(msg);
      });
    });
  });
});
