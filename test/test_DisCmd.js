const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { invoke } = require('lodash'),

  { waitEvent } = require('dim/test/utils'),
  { DnsServer, DicCmd, DisNode, DnsClient, NodeInfo } = require('dim'),
  { DisCmd, DimConf } = require('../src');

describe('DisCmd Node', function() {
  var env = {};

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    await env.dns.listen();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dns, 'close');
    env = {};
  });

  it('is loaded correctly', function(done) {
    const flow = [
      { id: "disCmd", type: "dim-cmd-server", name: "DIM Cmd Server",
        cnf: "conf", serviceName: "cmd/test", serviceDef: "I",
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DisCmd, DimConf ], flow, async function() {
      const disCmdNode = helper.getNode("disCmd");

      try {
        expect(disCmdNode).to.have.property('name', 'DIM Cmd Server');
        expect(disCmdNode.cnf).to.have.property('_host', 'localhost');
        expect(disCmdNode.cnf).to.have.property('_port', '2505');
        expect(disCmdNode.cnf).to.have.property('_retry', '1000');
        expect(disCmdNode).to.have.property('serviceName', 'cmd/test');
        expect(disCmdNode).to.have.property('serviceDef', 'I');
        expect(disCmdNode.wires).to.be.an('array')
        .that.deep.equal([ [ "help" ] ]);

        await waitEvent(disCmdNode.cnf.getDnsClient(), 'connect');
        expect(disCmdNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dimConf:dimConf.status.connected" } ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can receive a command', function(done) {
    const flow = [
      { id: "disCmd", type: "dim-cmd-server", name: "DIM Cmd Server",
        cnf: "conf", serviceName: "cmd/test", serviceDef: "I",
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DisCmd, DimConf ], flow, async function() {
      const disCmdNode = helper.getNode("disCmd");
      const helperNode = helper.getNode("help");

      helperNode.once('input', function(msg) {
        try {
          expect(disCmdNode.send.callCount).to.be.equal(1);
          expect(msg.payload).to.be.a('number').that.deep.equal(123);

          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(disCmdNode.cnf.getDnsClient(), 'connect');
        expect(disCmdNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dimConf:dimConf.status.connected" } ]);

        DicCmd.invoke('cmd/test', 123, env.dns.url());
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error if the Dns Configuration is not set', function(done) {
    const flow = [
      { id: "disCmd", type: "dim-cmd-server", name: "DIM Cmd Server",
        cnf: "conf", serviceName: "cmd/test", serviceDef: "I" }
    ];

    helper.load(DisCmd, flow, function() {
      const disCmdNode = helper.getNode("disCmd");
      try {
        expect(disCmdNode.error.lastCall.args).to.be.deep
        .equal([ 'node-red-dim/dimConf:dimConf.error.config-missing' ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error in case of duplicate taskNames', function(done) {
    const flow = [
      { id: "disCmd", type: "dim-cmd-server", name: "DIM Cmd Server",
        cnf: "conf", serviceName: "test/cmd1", serviceDef: "I" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dis = new DisNode();
    env.dis.addCmd('test/cmd2', 'I', (req) => req.value);
    let dnsClient = new DnsClient('localhost', 2505);
    dnsClient.register(env.dis).then(() => {
      dnsClient.unref();
      dnsClient = null;

      helper.load([ DisCmd, DimConf ], flow, function() {
        const disCmdNode = helper.getNode("disCmd");
        const taskName = disCmdNode.disNode.info.task;

        disCmdNode.cnf.on('call:error', function(call) {
          try {
            expect(call.args).to.be.deep.equal(
              [ 'dimConf.error.dup-taskName' ]);

            done();
          }
          catch (err) {
            done(err);
          }
        });

        try {
          expect(env.dis.info.task).to.be.deep.equal(taskName);
        }
        catch (err) {
          done(err);
        }
      });
    })
    .catch(done);
  });

  it('raises an error in case of duplicate services on different' +
  ' DIM Service Providers', function(done) {
    const flow = [
      { id: "disCmd", type: "dim-cmd-server", name: "DIM Cmd Server",
        cnf: "conf", serviceName: "test/cmd", serviceDef: "I" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dis = new DisNode(NodeInfo.local(0, 0, 'taskName2'));
    env.dis.addCmd('test/cmd', 'I', (req) => req.value);
    let dnsClient = new DnsClient('localhost', 2505);
    dnsClient.register(env.dis).then(() => {
      dnsClient.unref();
      dnsClient = null;

      helper.load([ DisCmd, DimConf ], flow, function() {
        const disCmdNode = helper.getNode("disCmd");

        disCmdNode.cnf.once('call:error', function(call) {
          try {
            expect(call.args).to.deep.equal([ 'dimConf.error.dup-service' ]);
            done();
          }
          catch (err) {
            done(err);
          }
        });
      });
    })
    .catch(done);
  });

  it('raises an error in case of duplicate services on the same' +
  ' DIM Service Provider', function(done) {
    const flow = [
      /* config node */
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      /* first node */
      { id: "disCmd1", type: "dim-cmd-server", name: "DIM Cmd Server 1",
        cnf: "conf", serviceName: "test/cmd", serviceDef: "I" },
      /* second node */
      { id: "disCmd2", type: "dim-cmd-server", name: "DIM Cmd Server 2",
        cnf: "conf", serviceName: "test/cmd", serviceDef: "I" }
    ];

    helper.load([ DisCmd, DimConf ], flow, function() {
      const disCmdNode1 = helper.getNode("disCmd1");
      const disCmdNode2 = helper.getNode("disCmd2");

      var f1 = function(call) {
        disCmdNode2.removeListener('call:error', f2);
        try {
          expect(call.args).to.be.deep.equal([ 'disCmd.error.dup-service' ]);
          done();
        }
        catch (err) {
          done(err);
        }
      };

      var f2 = function(call) {
        disCmdNode1.removeListener('call:error', f1);
        try {
          expect(call.args).to.be.deep.equal([ 'disCmd.error.dup-service' ]);
          done();
        }
        catch (err) {
          done(err);
        }
      };

      disCmdNode1.once('call:error', f1);
      disCmdNode2.once('call:error', f2);
    });
  });

  it('removes the CMD service when distroyed', function(done) {
    const flow = [
      { id: "disCmd", type: "dim-cmd-server", name: "DIM Cmd Server",
        cnf: "conf", serviceName: "cmd/test", serviceDef: "I" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DisCmd, DimConf ], flow, async function() {
      const disCmdNode = helper.getNode("disCmd");
      env.dis = disCmdNode.disNode;
      try {
        await waitEvent(disCmdNode.cnf.getDnsClient(), 'connect');

        expect(env.dis.isService('cmd/test')).to.be.true();
      }
      catch (err) {
        done(err);
      }

      helper.clearFlows() /* destroy the node */
      .then(() => {
        expect(env.dis.isService('cmd/test')).to.be.false();
        done();
      })
      .catch(done);
    });
  });
});
