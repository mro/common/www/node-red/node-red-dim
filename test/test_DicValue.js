/* eslint-disable max-lines*/
const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { invoke, now } = require('lodash'),
  { delay } = require('prom'),
  { waitEvent } = require('dim/test/utils'),
  { waitForValue } = require('./utils'),

  { DnsServer, DisNode, DnsClient } = require('dim'),
  { DicValue, DimConf } = require('../src');


describe('DicValue Node', function() {
  var env = {};

  beforeEach(async function() {
    env.dnsServer = new DnsServer('localhost', 2505);
    env.dis = new DisNode();
    env.srv = env.dis.addService('service/test', 'I', 123);
    await env.dnsServer.listen();
    const dnsClient = new DnsClient('localhost', 2505);
    await dnsClient.register(env.dis);
    dnsClient.unref();
    env.v = []; /* to collect values */
    env.ts = []; /* to collect timestamps */
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dnsServer, 'close');
    env = {};
  });

  it('is loaded correctly', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: true,
        scheduled: false, timeout: "5000", stamped: false, wires: [] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DicValue, DimConf ], flow, async function() {
      const dicValueNode = helper.getNode("dicValue");
      try {
        expect(dicValueNode).to.have.property('name', 'DIM Value Client');
        expect(dicValueNode.cnf).to.have.property('_host', 'localhost');
        expect(dicValueNode.cnf).to.have.property('_port', '2505');
        expect(dicValueNode.cnf).to.have.property('_retry', '1000');
        expect(dicValueNode).to.have.property('serviceName', 'service/test');
        expect(dicValueNode).to.have.property('monitored', true);
        expect(dicValueNode).to.have.property('scheduled', false);
        expect(dicValueNode).to.have.property('timeout', '5000');
        expect(dicValueNode).to.have.property('stamped', false);
        expect(dicValueNode.wires).to.be.an('array').that.is.empty();

        await waitEvent(dicValueNode.cnf.getDnsClient(), 'connect');
        expect(dicValueNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dimConf:dimConf.status.connected" } ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can monitor a value', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: true,
        scheduled: false, timeout: "5000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicValue, DimConf ], flow, async function() {
      const dicValueNode = helper.getNode("dicValue");
      const helperNode = helper.getNode("help");

      try {
        /* wait for the initial value */
        await waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return msg.payload;
        }, 123);

        await delay(400); /* ms */

        const prom = waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return msg.payload;
        }, 456);
        env.srv.setValue(456); /* update value */
        await prom;

        expect(dicValueNode.send.callCount).to.be.equal(2);
        expect(env.ts[1] - env.ts[0]).to.be.within(350, 450);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can poll a value', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: false,
        scheduled: true, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicValue, DimConf ], flow, async function() {
      const dicValueNode = helper.getNode("dicValue");
      const helperNode = helper.getNode("help");

      try {
        /* wait for the initial value */
        await waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          env.srv.setValue(456); /* update value */
          return msg.payload;
        }, 123);

        await waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return msg.payload;
        }, 456, 2000);

        expect(dicValueNode.send.callCount).to.be.equal(2);
        expect(env.ts[1] - env.ts[0]).to.be.within(950, 1050);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can retrieve a timestamped value', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: true,
        scheduled: false, timeout: "5000", stamped: true,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicValue, DimConf ], flow, function() {
      const helperNode = helper.getNode("help");

      helperNode.once('input', function(msg) {
        try {
          expect(msg.payload).to.be.a('number').that.deep.equal(123);
          expect(msg).to.have.property('timestamp');
          expect(msg.timestamp).to.be.closeTo(now(), 1000);
          done();
        }
        catch (err) {
          done(err);
        }
      });
    });
  });

  it('can monitor a value with timeout', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: true,
        scheduled: true, timeout: "1000", stamped: true,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicValue, DimConf ], flow, async function() {
      const dicValueNode = helper.getNode("dicValue");
      const helperNode = helper.getNode("help");

      /* wait for 3 value:
        1- initial value -> 123 (~ 0 ms)
        2- value updated -> 456 (~ 200 ms)
        3- timeout expired -> 456 (~ 1000 ms)
      */

      try {
        await waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return msg.payload;
        }, 123);

        await delay(200); /* ms */

        const prom = waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return msg.payload;
        }, 456);
        env.srv.setValue(456); /* update value */
        await prom;

        await waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return msg.payload;
        }, 456);

        expect(dicValueNode.send.callCount).to.be.equal(3);
        /* Interarrival time after update */
        expect(env.ts[1] - env.ts[0]).to.be.within(150, 250);
        /* Interarrival time after timeout */
        expect(env.ts[2] - env.ts[1]).to.be.within(750, 850);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can detect disconnections (monitored mode)', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: true,
        scheduled: false, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicValue, DimConf ], flow, async function() {
      const helperNode = helper.getNode("help");

      try {
        await waitForValue(helperNode, 'input', (msg) => msg.payload, 123);
        env.dis.close();
        await waitForValue(helperNode, 'input', (msg) => msg.payload,
          undefined);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can detect disconnections (scheduled mode)', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: false,
        scheduled: true, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicValue, DimConf ], flow, async function() {
      const helperNode = helper.getNode("help");

      try {
        await waitForValue(helperNode, 'input', (msg) => msg.payload, 123);
        env.dis.close();
        await waitForValue(helperNode, 'input', (msg) => msg.payload,
          undefined);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error if the Dns Configuration is not set', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: true,
        scheduled: false, timeout: "1000", stamped: false }
    ];

    helper.load(DicValue, flow, function() {
      const dicValueNode = helper.getNode("dicValue");

      try {
        expect(dicValueNode.error.lastCall.args).to.be.deep
        .equal([ 'node-red-dim/dimConf:dimConf.error.config-missing' ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error if the Dns Configuration is wrong', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: false,
        scheduled: false, timeout: "1000", stamped: false },
      { id: "conf", type: "dim-conf", host: "localhost", port: "1234",
        retry: "1000" }
    ];

    helper.load([ DicValue, DimConf ], flow, function() {
      const dicValueNode = helper.getNode("dicValue");

      dicValueNode.once('call:error', function(call) {
        try {
          expect(call.args.toString())
          .to.be.deep.equal('Error: failed to connect');

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });
  });

  it('can reconnect a service', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: false,
        scheduled: true, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicValue, DimConf ], flow, async function() {
      const helperNode = helper.getNode("help");

      helperNode.on('input', function(msg) {
        env.v.push(msg.payload);
      });

      try {
        await waitForValue(helperNode, 'input', (msg) => msg.payload, 123);

        const prom = waitEvent(env.dis, 'close');
        env.dis.close();
        await prom;

        /* restore the service */
        env.dis = new DisNode();
        env.srv = env.dis.addService('service/test', 'I', 456);
        const dnsClient = new DnsClient('localhost', 2505);
        await dnsClient.register(env.dis);
        dnsClient.unref();

        await waitForValue(helperNode, 'input', (msg) => msg.payload, 456);

        expect(env.v).to.be.deep.equal([ 123, undefined, 456 ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can retrieve a single value', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "service/test", monitored: false,
        scheduled: false, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicValue, DimConf ], flow, function() {
      const helperNode = helper.getNode("help");

      helperNode.on('input', function(msg) {
        try {
          expect(msg.payload).to.be.a('number').that.deep.equal(456);
          done();
        }
        catch (err) {
          done(err);
        }
      });

      env.srv.setValue(456); /* update value */

      helper.request().post('/dim-value-client/dicValue').expect(200)
      .end((err) => {
        if (err) {
          done(err);
        }
      });
    });
  });

  it('can request a non-existing service', function(done) {
    const flow = [
      { id: "dicValue", type: "dim-value-client", name: "DIM Value Client",
        cnf: "conf", serviceName: "SERVICE/TEST", monitored: false,
        scheduled: false, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicValue, DimConf ], flow, function() {
      const helperNode = helper.getNode("help");

      helperNode.on('input', function(msg) {
        try {
          expect(msg.payload).to.be.undefined();
          done();
        }
        catch (err) {
          done(err);
        }
      });

      helper.request().post('/dim-value-client/dicValue').expect(200)
      .end((err) => {
        if (err) {
          done(err);
        }
      });
    });
  });
});
