/* eslint-disable max-lines*/
const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, afterEach } = require('mocha'),

  { invoke, toString, pick } = require('lodash'),
  { waitEvent } = require('dim/test/utils'),
  { waitForValue } = require('./utils'),
  { DnsClient, ServiceInfo, NodeInfo, DisNode, DicDns,
    DicRpc, DicCmd } = require('dim'),
  { DnsServer } = require('../src');

describe('DnsServer Node', function() {
  var env = {};

  afterEach(async function() {
    await helper.unload();

    invoke(env.dis, 'close');
    invoke(env.disDns, 'close');

    invoke(env.dis2, 'close');
    invoke(env.disDns2, 'close');

    invoke(env.cli, 'close');
    env = {};
  });

  it('is loaded correctly', function(done) {
    const flow = [ { id: "dnsServer", type: "dim-dns-server",
      name: "DIM Dns Server", port: "2505", wires: [] } ];

    helper.load(DnsServer, flow, async function() {
      const dnsServerNode = helper.getNode("dnsServer");

      expect(dnsServerNode).to.have.property('name', 'DIM Dns Server');
      expect(dnsServerNode).to.have.property('_port', '2505');
      expect(dnsServerNode.wires).to.be.an('array').that.is.empty();

      try {
        await waitEvent(dnsServerNode, 'listening');
        expect(dnsServerNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dnsServer:dnsServer.status.ready" } ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('stops the server when destroyed', function(done) {
    const flow = [ { id: "dnsServer", type: "dim-dns-server",
      name: "DIM Dns Server", port: "2505" } ];

    helper.load(DnsServer, flow, async function() {
      const dnsServerNode = helper.getNode("dnsServer");

      try {
        await waitEvent(dnsServerNode, 'listening');
        expect(dnsServerNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dnsServer:dnsServer.status.ready" } ]);

        const prom = waitEvent(dnsServerNode._dnsServer, 'close');

        helper.clearFlows(); /* destroy the node */
        await prom;

        expect(dnsServerNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "red", shape: "dot",
            text: "node-red-dim/dnsServer:dnsServer.status.stopped" } ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can register/add/remove services', function(done) {
    const flow = [ { id: "dnsServer", type: "dim-dns-server",
      name: "DIM Dns Server", port: "2505", wires: [ [ "help" ] ] },
    { id: "help", type: "helper" } ];

    helper.load(DnsServer, flow, async function() {
      const dnsServerNode = helper.getNode("dnsServer");
      const helperNode = helper.getNode("help");
      env.dnsSrv = dnsServerNode._dnsServer;
      env.disDns = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
      env.cli = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
      env.dis = new DisNode();
      env.dis.addService('test_srv_1', 'I', 123);

      try {
        await waitEvent(dnsServerNode, 'listening');
        expect(dnsServerNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dnsServer:dnsServer.status.ready" } ]);

        /* register services */
        let prom = waitForValue(helperNode, 'input', (msg) => msg.payload,
          '+ test_srv_1');
        env.dis.register(env.disDns);
        await prom;

        /* add new service */
        prom = waitForValue(helperNode, 'input', (msg) => msg.payload,
          '+ test_srv_2');
        env.dis.addService('test_srv_2', 'F', 45.6);
        await prom;

        /* remove service */
        prom = waitForValue(helperNode, 'input', (msg) => msg.payload,
          '- test_srv_1');
        env.dis.removeService('test_srv_1');
        await prom;

        /* query services */
        let rep = await env.cli.query('test_srv_1');
        expect(rep).to.have.property('name', 'test_srv_1');
        expect(rep).to.have.own.property('node').and.instanceOf(NodeInfo);
        expect(rep.node.isValid()).to.be.false();

        rep = await env.cli.query('test_srv_2');
        expect(rep).to.have.property('name', 'test_srv_2');
        expect(toString(rep.definition)).to.be.equal('F');
        expect(rep).to.have.own.property('node').and.instanceOf(NodeInfo);
        expect(rep.node.isValid()).to.be.true();

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can unregister services', function(done) {
    const flow = [ { id: "dnsServer", type: "dim-dns-server",
      name: "DIM Dns Server", port: "2505", wires: [ [ "help" ] ] },
    { id: "help", type: "helper" } ];

    helper.load(DnsServer, flow, async function() {
      const dnsServerNode = helper.getNode("dnsServer");
      const helperNode = helper.getNode("help");
      env.dnsSrv = dnsServerNode._dnsServer;
      env.disDns = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
      env.cli = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
      env.dis = new DisNode();
      env.dis.addService('test_srv', 'I', 123);

      try {
        await waitEvent(dnsServerNode, 'listening');
        expect(dnsServerNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dnsServer:dnsServer.status.ready" } ]);

        let prom = waitForValue(helperNode, 'input', (msg) => msg.payload,
          '+ test_srv');
        env.dis.register(env.disDns);
        await prom;

        /* check service */
        let rep = await env.cli.query('test_srv');
        expect(rep).to.have.property('name', 'test_srv');
        expect(rep).to.have.own.property('node').and.instanceOf(NodeInfo);
        expect(rep.node.isValid()).to.be.true();

        /* unregister services */
        prom = waitEvent(env.cli, 'service:test_srv');
        env.disDns.unregister(env.dis);
        await prom;

        /* check again */
        rep = await env.cli.query('test_srv');
        expect(rep).to.have.property('name', 'test_srv');
        expect(rep).to.have.own.property('node').and.instanceOf(NodeInfo);
        expect(rep.node.isValid()).to.be.false();

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('unregisters services on DisNode close', function(done) {
    const flow = [ { id: "dnsServer", type: "dim-dns-server",
      name: "DIM Dns Server", port: "2505", wires: [ [ "help" ] ] },
    { id: "help", type: "helper" } ];

    helper.load(DnsServer, flow, async function() {
      const dnsServerNode = helper.getNode("dnsServer");
      const helperNode = helper.getNode("help");
      env.dnsSrv = dnsServerNode._dnsServer;
      env.disDns = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
      env.cli = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
      env.dis = new DisNode();
      env.dis.addService('test_srv', 'I', 123);

      try {
        await waitEvent(dnsServerNode, 'listening');
        expect(dnsServerNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dnsServer:dnsServer.status.ready" } ]);

        let prom = waitForValue(helperNode, 'input', (msg) => msg.payload,
          '+ test_srv');
        env.dis.register(env.disDns);
        await prom;

        /* check service */
        let rep = await env.cli.query('test_srv');
        expect(rep).to.have.property('name', 'test_srv');
        expect(rep).to.have.own.property('node').and.instanceOf(NodeInfo);
        expect(rep.node.isValid()).to.be.true();

        /* close DisNode */
        prom = waitEvent(env.cli, 'service:test_srv');
        env.dis.close();
        await prom;

        /* check again */
        rep = await env.cli.query('test_srv');
        expect(rep).to.have.property('name', 'test_srv');
        expect(rep).to.have.own.property('node').and.instanceOf(NodeInfo);
        expect(rep.node.isValid()).to.be.false();

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('unregisters services on DnsClient disconnection', function(done) {
    const flow = [ { id: "dnsServer", type: "dim-dns-server",
      name: "DIM Dns Server", port: "2505", wires: [ [ "help" ] ] },
    { id: "help", type: "helper" } ];

    helper.load(DnsServer, flow, async function() {
      const dnsServerNode = helper.getNode("dnsServer");
      const helperNode = helper.getNode("help");
      env.dnsSrv = dnsServerNode._dnsServer;
      env.disDns = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
      env.cli = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
      env.dis = new DisNode();
      env.dis.addService('test_srv', 'I', 123);

      try {
        await waitEvent(dnsServerNode, 'listening');
        expect(dnsServerNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dnsServer:dnsServer.status.ready" } ]);

        let prom = waitForValue(helperNode, 'input', (msg) => msg.payload,
          '+ test_srv');
        env.dis.register(env.disDns);
        await prom;

        /* check service */
        let rep = await env.cli.query('test_srv');
        expect(rep).to.have.property('name', 'test_srv');
        expect(rep).to.have.own.property('node').and.instanceOf(NodeInfo);
        expect(rep.node.isValid()).to.be.true();

        /* disconnect DnsClient */
        prom = waitEvent(env.cli, 'service:test_srv');
        env.disDns.close();
        await prom;

        /* check again */
        rep = await env.cli.query('test_srv');
        expect(rep).to.have.property('name', 'test_srv');
        expect(rep).to.have.own.property('node').and.instanceOf(NodeInfo);
        expect(rep.node.isValid()).to.be.false();

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('sends updates when a service is available / back available',
    function(done) {
      const flow = [ { id: "dnsServer", type: "dim-dns-server",
        name: "DIM Dns Server", port: "2505", wires: [ [ 'help' ] ] },
      { id: "help", type: "helper" } ];

      helper.load(DnsServer, flow, async function() {
        const dnsServerNode = helper.getNode("dnsServer");
        env.dnsSrv = dnsServerNode._dnsServer;
        env.disDns = new DnsClient(env.dnsSrv._conn.host,
          env.dnsSrv._conn.port);
        env.cli = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
        env.dis = new DisNode();
        env.dis.addService('test_srv', 'I', 123);

        try {
          await waitEvent(dnsServerNode, 'listening');

          /* query unknow service */
          let rep = await env.cli.query('test_srv');
          expect(rep)
          .to.be.deep.equal(new ServiceInfo('test_srv', '', 1, new NodeInfo()));

          let prom = waitEvent(env.cli, 'service:test_srv');
          env.dis.register(env.disDns); /* service becomes available */
          await prom;
          expect(dnsServerNode.send.lastCall.firstArg.payload)
          .to.be.deep.equal('+ test_srv');
          rep = await env.cli.query('test_srv');
          expect(rep).to.have.own.property('node').and.instanceOf(NodeInfo);
          expect(rep.node.isValid()).to.be.true();

          prom = waitEvent(env.cli, 'service:test_srv');
          env.dis.removeService('test_srv'); /* remove service */
          await prom;
          rep = await env.cli.query('test_srv');
          expect(rep)
          .to.be.deep.equal(new ServiceInfo('test_srv', '', 1, new NodeInfo()));

          prom = waitEvent(env.cli, 'service:test_srv');
          env.dis.addService('test_srv', 'I', 123); /* service back online */
          await prom;
          rep = await env.cli.query('test_srv');
          expect(rep).to.have.own.property('node').and.instanceOf(NodeInfo);
          expect(rep.node.isValid()).to.be.true();

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

  it('can serve request for non-existing services', function(done) {
    const flow = [ { id: "dnsServer", type: "dim-dns-server",
      name: "DIM Dns Server", port: "2505" } ];

    helper.load(DnsServer, flow, async function() {
      const dnsServerNode = helper.getNode("dnsServer");
      env.dnsSrv = dnsServerNode._dnsServer;
      env.cli = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);

      try {
        await waitEvent(dnsServerNode, 'listening');
        expect(dnsServerNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dnsServer:dnsServer.status.ready" } ]);

        const rep = await env.cli.query('test_srv');
        expect(rep)
        .to.be.deep.equal(new ServiceInfo('test_srv', '', 1, new NodeInfo()));

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('detects duplicate services', function(done) {
    const flow = [ { id: "dnsServer", type: "dim-dns-server",
      name: "DIM Dns Server", port: "2505" } ];

    helper.load(DnsServer, flow, async function() {
      const dnsServerNode = helper.getNode("dnsServer");
      env.dnsSrv = dnsServerNode._dnsServer;
      env.dis = new DisNode(NodeInfo.local(0, 0, 'taskName'));
      env.dis2 = new DisNode(NodeInfo.local(0, 0, 'taskName2'));
      env.disDns = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
      env.disDns2 = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);

      env.dis.addService('test_srv', 'I', 123);
      env.dis2.addService('test_srv', 'I', 123);
      try {
        await waitEvent(dnsServerNode, 'listening');

        await env.disDns.register(env.dis);
        const prom = waitEvent(env.dis2, 'duplicate:services');
        env.disDns2.register(env.dis2);
        const ret = await prom;

        expect(ret).to.be.deep.equal([ 'test_srv' ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('detects duplicate task names', function(done) {
    const flow = [ { id: "dnsServer", type: "dim-dns-server",
      name: "DIM Dns Server", port: "2505" } ];

    helper.load(DnsServer, flow, async function() {
      const dnsServerNode = helper.getNode("dnsServer");
      env.dnsSrv = dnsServerNode._dnsServer;
      env.dis = new DisNode(NodeInfo.local(0, 0, 'taskName'));
      env.dis2 = new DisNode(NodeInfo.local(0, 0, 'taskName'));
      env.disDns = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);
      env.disDns2 = new DnsClient(env.dnsSrv._conn.host, env.dnsSrv._conn.port);

      env.dis.addService('test_srv', 'I', 123);
      env.dis2.addService('test_srv', 'I', 123);
      try {
        await waitEvent(dnsServerNode, 'listening');

        await env.disDns.register(env.dis);
        const disDns2closure = waitEvent(env.disDns2, 'close');
        const prom = waitEvent(env.dis2, 'duplicate:taskName');
        env.disDns2.register(env.dis2);

        const ret = await prom;
        await disDns2closure;

        expect(ret).to.be.deep.equal(pick(env.dis.info,
          [ 'name', 'pid', 'port', 'task' ]));
        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  describe('can serve request for built-in services:', function() {
    it('DIS_DNS/SERVER_LIST', function(done) {
      const flow = [ { id: "dnsServer", type: "dim-dns-server",
        name: "DIM Dns Server", port: "2505" } ];

      helper.load(DnsServer, flow, async function() {
        const dnsServerNode = helper.getNode("dnsServer");
        env.dnsSrv = dnsServerNode._dnsServer;
        env.dis = new DisNode();

        try {
          await waitEvent(dnsServerNode, 'listening');

          await env.dis.register(env.dnsSrv.url());

          const val = await DicDns.serverList(env.dnsSrv.url());
          expect(val).to.be.deep.equal(
            [ { task: env.dis.info.task, node: env.dis.info.name,
              pid: env.dis.info.pid } ]);
          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('DIS_DNS/SERVICE_INFO (using service fullname or wildcard)',
      function(done) {
        const flow = [ { id: "dnsServer", type: "dim-dns-server",
          name: "DIM Dns Server", port: "2505" } ];

        helper.load(DnsServer, flow, async function() {
          const dnsServerNode = helper.getNode("dnsServer");
          env.dnsSrv = dnsServerNode._dnsServer;

          try {
            await waitEvent(dnsServerNode, 'listening');

            /* wildicard */
            let info = await DicRpc.invoke('DIS_DNS/SERVICE_INFO', '*',
              env.dnsSrv.url());
            expect(info).to.have.property('value').that.is.deep.equal(
              'DIS_DNS/SERVER_LIST|C|\n' +
              'DIS_DNS/KILL_SERVERS|I|CMD\n' +
              'DIS_DNS/SERVICE_INFO|C,C|RPC\n');

            /* wildicard postfix */
            info = await DicRpc.invoke('DIS_DNS/SERVICE_INFO', 'DIS_DNS/SERV*',
              env.dnsSrv.url());
            expect(info).to.have.property('value').that.is.deep.equal(
              'DIS_DNS/SERVER_LIST|C|\n' +
              'DIS_DNS/SERVICE_INFO|C,C|RPC\n');

            /* wildicard prefix */
            info = await DicRpc.invoke('DIS_DNS/SERVICE_INFO', '*_LIST',
              env.dnsSrv.url());
            expect(info).to.have.property('value').that.is.deep.equal(
              'DIS_DNS/SERVER_LIST|C|\n');

            /* fullname */
            info = await DicRpc.invoke('DIS_DNS/SERVICE_INFO',
              'DIS_DNS/KILL_SERVERS', env.dnsSrv.url());
            expect(info).to.have.property('value').that.is.deep.equal(
              'DIS_DNS/KILL_SERVERS|I|CMD\n');

            done();
          }
          catch (err) {
            done(err);
          }
        });
      });

    it('DIS_DNS/KILL_SERVERS', function(done) {
      const flow = [ { id: "dnsServer", type: "dim-dns-server",
        name: "DIM Dns Server", port: "2505" } ];

      helper.load(DnsServer, flow, async function() {
        const dnsServerNode = helper.getNode("dnsServer");
        env.dnsSrv = dnsServerNode._dnsServer;
        env.dis = new DisNode();
        env.dis.addService('test_srv', 'I', 123);

        try {
          await waitEvent(dnsServerNode, 'listening');

          await env.dis.register(env.dnsSrv.url());

          const prom = waitEvent(env.dis, 'exit');
          DicCmd.invoke('DIS_DNS/KILL_SERVERS', [], env.dnsSrv.url());
          await prom;

          const val = await DicDns.serverList(env.dnsSrv.url());
          expect(val).to.be.empty();

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });
  });
});
