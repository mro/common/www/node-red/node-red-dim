const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { invoke } = require('lodash'),

  { waitEvent } = require('dim/test/utils'),
  { waitForValue } = require('./utils'),
  { DnsServer, DicValue, DisNode, DnsClient, NodeInfo } = require('dim'),
  { DisValue, DimConf } = require('../src');

describe('DisValue Node', function() {
  var env = {};

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    await env.dns.listen();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dns, 'close');
    invoke(env.dis, 'close');
    invoke(env.dicValue, 'release');
    env = {};
  });

  it('is loaded correctly', function(done) {
    const flow = [
      { id: "disValue", type: "dim-value-server", name: "DIM Value Server",
        cnf: "conf", serviceName: "service/test", serviceDef: "I",
        value: "123", wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DisValue, DimConf ], flow, async function() {
      const disValueNode = helper.getNode("disValue");

      try {
        expect(disValueNode).to.have.property('name', 'DIM Value Server');
        expect(disValueNode.cnf).to.have.property('_host', 'localhost');
        expect(disValueNode.cnf).to.have.property('_port', '2505');
        expect(disValueNode.cnf).to.have.property('_retry', '1000');
        expect(disValueNode).to.have.property('serviceName', 'service/test');
        expect(disValueNode).to.have.property('serviceDef', 'I');
        expect(disValueNode).to.have.property('value', '123');
        expect(disValueNode.wires).to.be.an('array')
        .that.deep.equal([ [ "help" ] ]);

        await waitEvent(disValueNode.cnf.getDnsClient(), 'connect');
        expect(disValueNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dimConf:dimConf.status.connected" } ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can send updates', function(done) {
    const service = 'service/test';
    const flow = [
      { id: "disValue", type: "dim-value-server", name: "DIM Value Server",
        cnf: "conf", serviceName: service, serviceDef: "I",
        value: "123", wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    env.dicValue = new DicValue(service, null, env.dns.url());

    helper.load([ DisValue, DimConf ], flow, function() {
      const disValueNode = helper.getNode("disValue");
      const helperNode = helper.getNode("help");
      /* Check initial value */
      env.dicValue.then((val) => expect(val).to.be.deep.equal(123))
      .then(async function() {
        const prom = waitForValue(env.dicValue, 'value', (val) => val, 456);

        helperNode.once('input', async function(msg) {
          try {
            expect(msg.payload).to.be.deep.equal(456);
            await prom;

            done();
          }
          catch (err) {
            done(err);
          }
        });

        /* update value */
        disValueNode.receive({ payload: 456 });
      })
      .catch(done);
    });
  });

  it('raises an error if the Dns Configuration is not set', function(done) {
    const flow = [
      { id: "disValue", type: "dim-value-server", name: "DIM Value Server",
        cnf: "conf", serviceName: "service/test", serviceDef: "I",
        value: "123" }
    ];

    helper.load(DisValue, flow, function() {
      const disValueNode = helper.getNode("disValue");
      try {
        expect(disValueNode.error.lastCall.args).to.be.deep
        .equal([ 'node-red-dim/dimConf:dimConf.error.config-missing' ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error in case of duplicate taskNames', function(done) {
    const flow = [
      { id: "disValue", type: "dim-value-server", name: "DIM Value Server",
        cnf: "conf", serviceName: "test/srv1", serviceDef: "I",
        value: "123" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dis = new DisNode();
    env.dis.addService('test/srv2', 'I', 123);
    let dnsClient = new DnsClient('localhost', 2505);
    dnsClient.register(env.dis).then(() => {
      dnsClient.unref();
      dnsClient = null;

      helper.load([ DisValue, DimConf ], flow, function() {
        const disValueNode = helper.getNode("disValue");
        const taskName = disValueNode.disNode.info.task;

        disValueNode.cnf.on('call:error', function(call) {
          try {
            expect(call.args).to.be.deep.equal(
              [ 'dimConf.error.dup-taskName' ]);
            done();
          }
          catch (err) {
            done(err);
          }
        });

        try {
          expect(env.dis.info.task).to.be.deep.equal(taskName);
        }
        catch (err) {
          done(err);
        }
      });
    })
    .catch(done);
  });

  it('raises an error in case of duplicate services on different' +
  ' DIM Service Providers', function(done) {
    const flow = [
      { id: "disValue", type: "dim-value-server", name: "DIM Value Server",
        cnf: "conf", serviceName: "test/srv", serviceDef: "I",
        value: "123" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dis = new DisNode(NodeInfo.local(0, 0, 'taskName2'));
    env.dis.addService('test/srv', 'I', 123);
    let dnsClient = new DnsClient('localhost', 2505);
    dnsClient.register(env.dis).then(() => {
      dnsClient.unref();
      dnsClient = null;

      helper.load([ DisValue, DimConf ], flow, function() {
        const disValueNode = helper.getNode("disValue");

        disValueNode.cnf.once('call:error', function(call) {
          try {
            expect(call.args).to.deep.equal([ 'dimConf.error.dup-service' ]);

            done();
          }
          catch (err) {
            done(err);
          }
        });
      });
    })
    .catch(done);
  });

  it('raises an error in case of duplicate services on the same' +
  ' DIM Service Provider', function(done) {
    const flow = [
      /* config node */
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      /* first node */
      { id: "disValue1", type: "dim-value-server", name: "DIM Value Server 1",
        cnf: "conf", serviceName: "test/srv", serviceDef: "I",
        value: "123" },
      /* second node */
      { id: "disValue2", type: "dim-value-server", name: "DIM Value Server 2",
        cnf: "conf", serviceName: "test/srv", serviceDef: "I",
        value: "123" }
    ];

    helper.load([ DisValue, DimConf ], flow, function() {
      const disValueNode1 = helper.getNode("disValue1");
      const disValueNode2 = helper.getNode("disValue2");

      var f1 = function(call) {
        disValueNode2.removeListener('call:error', f2);

        try {
          expect(call.args).to.be.deep.equal(
            [ 'disValue.error.dup-service' ]);
          done();
        }
        catch (err) {
          done(err);
        }
      };

      var f2 = function(call) {
        disValueNode1.removeListener('call:error', f1);

        try {
          expect(call.args).to.be.deep.equal(
            [ 'disValue.error.dup-service' ]);
          done();
        }
        catch (err) {
          done(err);
        }
      };

      disValueNode1.once('call:error', f1);
      disValueNode2.once('call:error', f2);
    });
  });

  it('removes the service when distroyed', function(done) {
    const flow = [
      { id: "disValue", type: "dim-value-server", name: "DIM Value Server",
        cnf: "conf", serviceName: "service/test", serviceDef: "I",
        value: "123" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DisValue, DimConf ], flow, function() {
      const disValueNode = helper.getNode("disValue");
      env.dis = disValueNode.disNode;

      try {
        expect(env.dis.isService('service/test')).to.be.true();
      }
      catch (err) {
        done(err);
      }

      helper.clearFlows() /* destroy the node */
      .then(() => {
        expect(env.dis.isService('service/test')).to.be.false();
        done();
      })
      .catch(done);
    });
  });
});
