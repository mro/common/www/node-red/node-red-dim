const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { invoke } = require('lodash'),
  { delay } = require('prom'),

  { waitEvent } = require('dim/test/utils'),
  { DnsServer, DisNode, DnsClient } = require('dim'),
  { DicRpc, DimConf } = require('../src');

describe('DicRpc Node', function() {
  var env = {};

  beforeEach(async function() {
    env.dnsServer = new DnsServer('localhost', 2505);
    env.dis = new DisNode();
    env.dis.addRpc('rpc/test', 'I,C', (req) => ('value: ' + (req.value + 1)));
    await env.dnsServer.listen();
    const dnsClient = new DnsClient('localhost', 2505);
    await dnsClient.register(env.dis);
    dnsClient.unref();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dnsServer, 'close');
    env = {};
  });

  it('is loaded correctly', function(done) {
    const flow = [
      { id: "dicRpc", type: "dim-rpc-client", name: "DIM Rpc Client",
        cnf: "conf", serviceName: "rpc/test", timeout: "1000", wires: [] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DicRpc, DimConf ], flow, async function() {
      const dicRpcNode = helper.getNode("dicRpc");
      try {
        expect(dicRpcNode).to.have.property('name', 'DIM Rpc Client');
        expect(dicRpcNode.cnf).to.have.property('_host', 'localhost');
        expect(dicRpcNode.cnf).to.have.property('_port', '2505');
        expect(dicRpcNode.cnf).to.have.property('_retry', '1000');
        expect(dicRpcNode).to.have.property('serviceName', 'rpc/test');
        expect(dicRpcNode).to.have.property('timeout', '1000');
        expect(dicRpcNode.wires).to.be.an('array').that.is.empty();

        await waitEvent(dicRpcNode.cnf.getDnsClient(), 'connect');
        expect(dicRpcNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dimConf:dimConf.status.connected" } ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can send an RPC request', function(done) {
    const value = 123;
    const flow = [
      { id: "dicRpc", type: "dim-rpc-client", name: "DIM Rpc Client",
        cnf: "conf", serviceName: "rpc/test", timeout: "1000",
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicRpc, DimConf ], flow, function() {
      const dicRpcNode = helper.getNode("dicRpc");
      const helperNode = helper.getNode("help");

      helperNode.once('input', function(msg) {
        try {
          expect(dicRpcNode.send.callCount).to.be.equal(1);

          expect(msg.payload.value).to.be.a('string')
          .that.deep.equal('value: ' + (value + 1));

          done();
        }
        catch (err) {
          done(err);
        }
      });

      dicRpcNode.receive({ payload: value });
    });
  });

  it('raises an error if the Dns Configuration is not set', function(done) {
    const flow = [
      { id: "dicRpc", type: "dim-rpc-client", name: "DIM Rpc Client",
        cnf: "conf", serviceName: "rpc/test", timeout: "1000" }
    ];

    helper.load(DicRpc, flow, function() {
      const dicRpcNode = helper.getNode("dicRpc");
      try {
        expect(dicRpcNode.error.lastCall.args).to.be.deep
        .equal([ 'node-red-dim/dimConf:dimConf.error.config-missing' ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error if the Dns Configuration is wrong', function(done) {
    const flow = [
      { id: "dicRpc", type: "dim-rpc-client", name: "DIM Rpc Client",
        cnf: "conf", serviceName: "rpc/test", timeout: "1000" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "1234",
        retry: "1000" }
    ];

    helper.load([ DicRpc, DimConf ], flow, function() {
      const dicRpcNode = helper.getNode("dicRpc");

      dicRpcNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: failed to connect');

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });
  });

  it('raises an error if service doesn\'t exist', function(done) {
    const flow = [
      { id: "dicRpc", type: "dim-rpc-client", name: "DIM Rpc Client",
        cnf: "conf", serviceName: "test/rpc", timeout: "1000" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DicRpc, DimConf ], flow, async function() {
      const dicRpcNode = helper.getNode("dicRpc");

      dicRpcNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: insufficient service information');

          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        await waitEvent(dicRpcNode.cnf.getDnsClient(), 'connect');
        expect(dicRpcNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dimConf:dimConf.status.connected" } ]);
        dicRpcNode.receive({ payload: 123 });
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error if reply isn\'t received before timeout', function(done) {
    const flow = [
      { id: "dicRpc", type: "dim-rpc-client", name: "DIM Rpc Client",
        cnf: "conf", serviceName: "rpc/test_1", timeout: "1000" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    /* add a service with a reply delay of 1.5 seconds */
    env.dis.addRpc('rpc/test_1', 'I,I', async function(req) {
      await delay(1500); /* ms */
      return req.value + 1;
    });

    helper.load([ DicRpc, DimConf ], flow, function() {
      const dicRpcNode = helper.getNode("dicRpc");

      dicRpcNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: request time-out');

          done();
        }
        catch (err) {
          done(err);
        }
      });

      dicRpcNode.receive({ payload: 123 });
    });
  });
});
