/* eslint-disable max-lines*/
const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { invoke } = require('lodash'),

  { waitEvent } = require('dim/test/utils'),
  { waitForValue } = require('./utils'),
  { DnsServer, DicValue, DisNode, DnsClient, NodeInfo } = require('dim'),
  { DisJson, DimConf } = require('../src');

describe('DisJson Node', function() {
  var env = {};

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    await env.dns.listen();
    env.value = {
      num: 123,
      str: 'test',
      arr: [ 4, 5, 6 ],
      bool: true,
      nil: null,
      obj: { 0: 'test' }
    };
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dns, 'close');
    invoke(env.dis, 'close');
    invoke(env.dicValue, 'release');
    env = {};
  });

  it('is loaded correctly', function(done) {
    const flow = [
      { id: "disJson", type: "dim-json-server", name: "DIM JSON Server",
        cnf: "conf", serviceName: "service/json", value: "{}",
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DisJson, DimConf ], flow, async function() {
      const disJsonNode = helper.getNode("disJson");

      try {
        expect(disJsonNode).to.have.property('name', 'DIM JSON Server');
        expect(disJsonNode.cnf).to.have.property('_host', 'localhost');
        expect(disJsonNode.cnf).to.have.property('_port', '2505');
        expect(disJsonNode.cnf).to.have.property('_retry', '1000');
        expect(disJsonNode).to.have.property('serviceName', 'service/json');
        expect(disJsonNode).to.have.property('value', '{}');
        expect(disJsonNode.wires).to.be.an('array')
        .that.deep.equal([ [ "help" ] ]);

        await waitEvent(disJsonNode.cnf.getDnsClient(), 'connect');
        expect(disJsonNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dimConf:dimConf.status.connected" } ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can send updates in JSON format', function(done) {
    const service = 'service/json';
    const flow = [
      { id: "disJson", type: "dim-json-server", name: "DIM JSON Server",
        cnf: "conf", serviceName: "service/json", value: "{}",
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    env.dicValue = new DicValue(service, null, env.dns.url());

    helper.load([ DisJson, DimConf ], flow, function() {
      const disJsonNode = helper.getNode("disJson");
      const helperNode = helper.getNode("help");
      /* Check initial value */
      env.dicValue.then((val) => expect(val).to.be.deep.equal("{}"))
      .then(async function() {
        const prom = waitForValue(env.dicValue, 'value', (val) => val,
          JSON.stringify(env.value));

        helperNode.once('input', async function(msg) {
          try {
            expect(msg.payload).to.be.deep.equal(env.value);
            await prom;

            done();
          }
          catch (err) {
            done(err);
          }
        });

        /* update value */
        disJsonNode.receive({ payload: env.value });
      })
      .catch(done);
    });
  });

  it('can report JSON conversion errors', function(done) {
    const service = 'service/json';
    const flow = [
      { id: "disJson", type: "dim-json-server", name: "DIM JSON Server",
        cnf: "conf", serviceName: "service/json", value: "{}" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dicValue = new DicValue(service, null, env.dns.url());

    helper.load([ DisJson, DimConf ], flow, function() {
      const disJsonNode = helper.getNode("disJson");
      const value = {
        num: 123,
        toJSON: () => { throw new Error('JSON conversion error'); }
      };

      disJsonNode.on('call:error', function(call) {
        try {
          expect(call.firstArg)
          .to.be.deep.equal("Error: JSON conversion error");
          expect(call.lastArg.payload).to.be.deep.equal(value);

          done();
        }
        catch (err) {
          done(err);
        }
      });

      /* update value */
      disJsonNode.receive({ payload: value });
    });
  });

  it('raises an error if the Dns Configuration is not set', function(done) {
    const flow = [
      { id: "disJson", type: "dim-json-server", name: "DIM JSON Server",
        cnf: "conf", serviceName: "service/json", value: "{}" }
    ];

    helper.load(DisJson, flow, function() {
      const disJsonNode = helper.getNode("disJson");
      try {
        expect(disJsonNode.error.lastCall.args).to.be.deep
        .equal([ 'node-red-dim/dimConf:dimConf.error.config-missing' ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error in case of duplicate taskNames', function(done) {
    const flow = [
      { id: "disJson", type: "dim-json-server", name: "DIM JSON Server",
        cnf: "conf", serviceName: "service/json1", value: "{}" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dis = new DisNode();
    env.dis.addService('service/json2', 'C', '{}');
    let dnsClient = new DnsClient('localhost', 2505);
    dnsClient.register(env.dis).then(() => {
      dnsClient.unref();
      dnsClient = null;

      helper.load([ DisJson, DimConf ], flow, function() {
        const disJsonNode = helper.getNode("disJson");
        const taskName = disJsonNode.disNode.info.task;

        disJsonNode.cnf.on('call:error', function(call) {
          try {
            expect(call.args).to.be.deep.equal(
              [ 'dimConf.error.dup-taskName' ]);
            done();
          }
          catch (err) {
            done(err);
          }
        });

        try {
          expect(env.dis.info.task).to.be.deep.equal(taskName);
        }
        catch (err) {
          done(err);
        }
      });
    })
    .catch(done);
  });

  it('raises an error in case of duplicate services on different' +
  ' DIM Service Providers', function(done) {
    const flow = [
      { id: "disJson", type: "dim-json-server", name: "DIM JSON Server",
        cnf: "conf", serviceName: "service/json", value: "{}" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    env.dis = new DisNode(NodeInfo.local(0, 0, 'taskName2'));
    env.dis.addService('service/json', 'C', '{}');
    let dnsClient = new DnsClient('localhost', 2505);
    dnsClient.register(env.dis).then(() => {
      dnsClient.unref();
      dnsClient = null;

      helper.load([ DisJson, DimConf ], flow, function() {
        const disJsonNode = helper.getNode("disJson");

        disJsonNode.cnf.once('call:error', function(call) {
          try {
            expect(call.args).to.deep.equal([ 'dimConf.error.dup-service' ]);

            done();
          }
          catch (err) {
            done(err);
          }
        });
      });
    })
    .catch(done);
  });

  it('raises an error in case of duplicate services on the same' +
  ' DIM Service Provider', function(done) {
    const flow = [
      /* config node */
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      /* first node */
      { id: "disJson1", type: "dim-json-server", name: "DIM JSON Server 1",
        cnf: "conf", serviceName: "service/json", value: "{}" },
      /* second node */
      { id: "disJson2", type: "dim-json-server", name: "DIM JSON Server 2",
        cnf: "conf", serviceName: "service/json", value: "{}" }
    ];

    helper.load([ DisJson, DimConf ], flow, function() {
      const disJsonNode1 = helper.getNode("disJson1");
      const disJsonNode2 = helper.getNode("disJson2");

      var f1 = function(call) {
        disJsonNode2.removeListener('call:error', f2);

        try {
          expect(call.args).to.be.deep.equal(
            [ 'disValue.error.dup-service' ]);
          done();
        }
        catch (err) {
          done(err);
        }
      };

      var f2 = function(call) {
        disJsonNode1.removeListener('call:error', f1);

        try {
          expect(call.args).to.be.deep.equal(
            [ 'disValue.error.dup-service' ]);
          done();
        }
        catch (err) {
          done(err);
        }
      };

      disJsonNode1.once('call:error', f1);
      disJsonNode2.once('call:error', f2);
    });
  });

  it('removes the service when distroyed', function(done) {
    const flow = [
      { id: "disJson", type: "dim-json-server", name: "DIM JSON Server",
        cnf: "conf", serviceName: "service/json", value: "{}" },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DisJson, DimConf ], flow, function() {
      const disJsonNode = helper.getNode("disJson");
      env.dis = disJsonNode.disNode;

      try {
        expect(env.dis.isService('service/json')).to.be.true();
      }
      catch (err) {
        done(err);
      }

      helper.clearFlows() /* destroy the node */
      .then(() => {
        expect(env.dis.isService('service/json')).to.be.false();
        done();
      })
      .catch(done);
    });
  });
});
