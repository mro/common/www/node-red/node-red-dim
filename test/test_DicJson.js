/* eslint-disable max-lines*/
const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),
  { invoke, now, isEqual } = require('lodash'),
  { delay } = require('prom'),
  { waitEvent } = require('dim/test/utils'),
  { waitForValue } = require('./utils'),

  { DnsServer, DisNode, DnsClient } = require('dim'),
  { DicJson, DimConf } = require('../src');

describe('DicJson Node', function() {
  var env = {};

  beforeEach(async function() {
    env.dnsServer = new DnsServer('localhost', 2505);
    env.dis = new DisNode();
    env.srv = env.dis.addService('service/json', 'C', '{}');
    await env.dnsServer.listen();
    const dnsClient = new DnsClient('localhost', 2505);
    await dnsClient.register(env.dis);
    dnsClient.unref();
    env.v = []; /* to collect values */
    env.ts = []; /* to collect timestamps */
    env.value = {
      num: 123,
      str: 'test',
      arr: [ 4, 5, 6 ],
      bool: true,
      nil: null,
      obj: { 0: 'test' }
    };
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dis, 'close');
    invoke(env.dnsServer, 'close');
    env = {};
  });

  it('is loaded correctly', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: true,
        scheduled: false, timeout: "5000", stamped: false, wires: [] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DicJson, DimConf ], flow, async function() {
      const dicJsonNode = helper.getNode("dicJson");
      try {
        expect(dicJsonNode).to.have.property('name', 'DIM JSON Client');
        expect(dicJsonNode.cnf).to.have.property('_host', 'localhost');
        expect(dicJsonNode.cnf).to.have.property('_port', '2505');
        expect(dicJsonNode.cnf).to.have.property('_retry', '1000');
        expect(dicJsonNode).to.have.property('serviceName', 'service/json');
        expect(dicJsonNode).to.have.property('monitored', true);
        expect(dicJsonNode).to.have.property('scheduled', false);
        expect(dicJsonNode).to.have.property('timeout', '5000');
        expect(dicJsonNode).to.have.property('stamped', false);
        expect(dicJsonNode.wires).to.be.an('array').that.is.empty();

        await waitEvent(dicJsonNode.cnf.getDnsClient(), 'connect');
        expect(dicJsonNode.status.lastCall.args).to.be.deep.equal(
          [ { fill: "green", shape: "dot",
            text: "node-red-dim/dimConf:dimConf.status.connected" } ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can monitor a JSON string value', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: true,
        scheduled: false, timeout: "5000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicJson, DimConf ], flow, async function() {
      const dicJsonNode = helper.getNode("dicJson");
      const helperNode = helper.getNode("help");

      try {
        /* wait for the initial value */
        await waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return isEqual(msg.payload, {});
        }, true);

        await delay(400); /* ms */

        const prom = waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return isEqual(msg.payload, env.value);
        }, true);
        env.srv.setValue(JSON.stringify(env.value)); /* update value */
        await prom;

        expect(dicJsonNode.send.callCount).to.be.equal(2);
        expect(env.ts[1] - env.ts[0]).to.be.within(350, 450);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can poll a JSON string value', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: false,
        scheduled: true, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicJson, DimConf ], flow, async function() {
      const dicJsonNode = helper.getNode("dicJson");
      const helperNode = helper.getNode("help");

      try {
        /* wait for the initial value */
        await waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          env.srv.setValue(JSON.stringify(env.value)); /* update value */
          return isEqual(msg.payload, {});
        }, true);

        await waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return isEqual(msg.payload, env.value);
        }, true, 2000);

        expect(dicJsonNode.send.callCount).to.be.equal(2);
        expect(env.ts[1] - env.ts[0]).to.be.within(950, 1050);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can retrieve a timestamped JSON string value', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: true,
        scheduled: false, timeout: "5000", stamped: true,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicJson, DimConf ], flow, function() {
      const helperNode = helper.getNode("help");

      helperNode.once('input', function(msg) {
        try {
          expect(msg.payload).to.be.a('object').that.deep.equal({});
          expect(msg).to.have.property('timestamp');
          expect(msg.timestamp).to.be.closeTo(now(), 1000);
          done();
        }
        catch (err) {
          done(err);
        }
      });
    });
  });

  it('can monitor a JSON string value with timeout', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: true,
        scheduled: true, timeout: "1000", stamped: true,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicJson, DimConf ], flow, async function() {
      const dicJsonNode = helper.getNode("dicJson");
      const helperNode = helper.getNode("help");

      /* wait for 3 value:
        1- initial value -> 123 (~ 0 ms)
        2- value updated -> 456 (~ 200 ms)
        3- timeout expired -> 456 (~ 1000 ms)
      */

      try {
        await waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return isEqual(msg.payload, {});
        }, true);

        await delay(200); /* ms */

        const prom = waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return isEqual(msg.payload, env.value);
        }, true);
        env.srv.setValue(JSON.stringify(env.value)); /* update value */
        await prom;

        await waitForValue(helperNode, 'input', (msg) => {
          env.ts.push(now());
          return isEqual(msg.payload, env.value);
        }, true);

        expect(dicJsonNode.send.callCount).to.be.equal(3);
        /* Interarrival time after update */
        expect(env.ts[1] - env.ts[0]).to.be.within(150, 250);
        /* Interarrival time after timeout */
        expect(env.ts[2] - env.ts[1]).to.be.within(750, 850);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can detect disconnections (monitored mode)', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: true,
        scheduled: false, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicJson, DimConf ], flow, async function() {
      const helperNode = helper.getNode("help");

      try {
        await waitForValue(helperNode, 'input',
          (msg) => isEqual(msg.payload, {}), true);
        env.dis.close();
        await waitForValue(helperNode, 'input',
          (msg) => isEqual(msg.payload, undefined), true);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can detect disconnections (scheduled mode)', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: false,
        scheduled: true, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicJson, DimConf ], flow, async function() {
      const helperNode = helper.getNode("help");

      try {
        await waitForValue(helperNode, 'input',
          (msg) => isEqual(msg.payload, {}), true);
        env.dis.close();
        await waitForValue(helperNode, 'input',
          (msg) => isEqual(msg.payload, undefined), true);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error if the Dns Configuration is not set', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: true,
        scheduled: false, timeout: "1000", stamped: false }
    ];

    helper.load(DicJson, flow, function() {
      const dicJsonNode = helper.getNode("dicJson");

      try {
        expect(dicJsonNode.error.lastCall.args).to.be.deep
        .equal([ 'node-red-dim/dimConf:dimConf.error.config-missing' ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises an error if the Dns Configuration is wrong', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: false,
        scheduled: false, timeout: "1000", stamped: false },
      { id: "conf", type: "dim-conf", host: "localhost", port: "1234",
        retry: "1000" }
    ];

    helper.load([ DicJson, DimConf ], flow, function() {
      const dicJsonNode = helper.getNode("dicJson");

      dicJsonNode.once('call:error', function(call) {
        try {
          expect(call.args.toString())
          .to.be.deep.equal('Error: failed to connect');

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });
  });

  it('can reconnect a service', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: false,
        scheduled: true, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicJson, DimConf ], flow, async function() {
      const helperNode = helper.getNode("help");

      helperNode.on('input', function(msg) {
        env.v.push(msg.payload);
      });

      try {
        await waitForValue(helperNode, 'input',
          (msg) => isEqual(msg.payload, {}), true);

        const prom = waitEvent(env.dis, 'close');
        env.dis.close();
        await prom;

        /* restore the service */
        env.dis = new DisNode();
        env.srv = env.dis.addService('service/json', 'C',
          JSON.stringify(env.value));
        const dnsClient = new DnsClient('localhost', 2505);
        await dnsClient.register(env.dis);
        dnsClient.unref();

        await waitForValue(helperNode, 'input',
          (msg) => isEqual(msg.payload, env.value), true);

        expect(env.v).to.be.deep.equal([ {}, undefined, env.value ]);

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can retrieve a single JSON string value', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: false,
        scheduled: false, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicJson, DimConf ], flow, function() {
      const helperNode = helper.getNode("help");

      helperNode.on('input', function(msg) {
        try {
          expect(msg.payload).to.be.a('object').that.deep
          .equal(env.value);
          done();
        }
        catch (err) {
          done(err);
        }
      });

      /* update value */
      env.srv.setValue(JSON.stringify(env.value));

      helper.request().post('/dim-json-client/dicJson').expect(200)
      .end((err) => {
        if (err) {
          done(err);
        }
      });
    });
  });

  it('can report JSON parsing errors', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "service/json", monitored: false,
        scheduled: false, timeout: "1000", stamped: false },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" }
    ];

    helper.load([ DicJson, DimConf ], flow, function() {
      const dicJsonNode = helper.getNode("dicJson");

      dicJsonNode.once('call:error', function(call) {
        try {
          expect(call.firstArg).to.have.a.string('SyntaxError');
          expect(call.lastArg.payload).to.be.deep.equal('{ "str":test }');

          done();
        }
        catch (err) {
          done(err);
        }
      });

      /* update value */
      env.srv.setValue('{ "str":test }');

      helper.request().post('/dim-json-client/dicJson').expect(200)
      .end((err) => {
        if (err) {
          done(err);
        }
      });
    });
  });

  it('can request a non-existing service', function(done) {
    const flow = [
      { id: "dicJson", type: "dim-json-client", name: "DIM JSON Client",
        cnf: "conf", serviceName: "SERVICE/JSON", monitored: false,
        scheduled: false, timeout: "1000", stamped: false,
        wires: [ [ "help" ] ] },
      { id: "conf", type: "dim-conf", host: "localhost", port: "2505",
        retry: "1000" },
      { id: "help", type: "helper" }
    ];

    helper.load([ DicJson, DimConf ], flow, function() {
      const helperNode = helper.getNode("help");

      helperNode.on('input', function(msg) {
        try {
          expect(msg.payload).to.be.undefined();
          done();
        }
        catch (err) {
          done(err);
        }
      });

      helper.request().post('/dim-json-client/dicJson').expect(200)
      .end((err) => {
        if (err) {
          done(err);
        }
      });
    });
  });
});
