/* eslint-disable max-lines*/
const
  { expect } = require('chai'),
  helper = require('node-red-node-test-helper'),
  { describe, it, beforeEach, afterEach } = require('mocha'),

  os = require('os'),
  { invoke } = require('lodash'),
  { waitEvent } = require('dim/test/utils'),
  { waitForValue } = require('./utils'),
  { DnsServer, ServiceInfo, DisNode, ServiceDefinition,
    DicValue, NodeInfo, DnsClient, DicCmd } = require('dim'),
  { VersionNumber } = require('dim/src/Consts'),
  { DimConf } = require('../src');


describe('DimConf Node', function() {
  var env = {};
  /* TASK_MAX and NAME_MAX include trailing \0 */
  const task = os.hostname().slice(0, NodeInfo.TASK_MAX - 1);
  const node = os.hostname().slice(0, NodeInfo.NAME_MAX - 1);

  beforeEach(async function() {
    env.dns = new DnsServer('localhost', 2505);
    await env.dns.listen();
  });

  afterEach(async function() {
    await helper.unload();
    invoke(env.dns, 'close');
    invoke(env.dicValue, 'release');
    env = {};
  });

  it('is loaded correctly', function(done) {
    const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
      port: "1234", retry: "5678" } ];

    helper.load(DimConf, flow, function() {
      const confNode = helper.getNode("conf");

      try {
        expect(confNode).to.have.property('_host', 'localhost');
        expect(confNode).to.have.property('_port', '1234');
        expect(confNode).to.have.property('_retry', '5678');

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can query a service', function(done) {
    const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
      port: "2505" } ];

    helper.load(DimConf, flow, async function() {
      const confNode = helper.getNode("conf");
      const dnsClient = confNode.getDnsClient();

      try {
        const srv = await dnsClient.query('DIS_DNS/SERVER_LIST');
        expect(srv).to.be.instanceOf(ServiceInfo);
        expect(srv.definition).to.be.deep.equal(ServiceDefinition.parse('C'));

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can spawn a DIM Service Provider', function(done) {
    const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
      port: "2505" } ];

    helper.load(DimConf, flow, async function() {
      const confNode = helper.getNode("conf");

      /* fetch value from a DIS built-in service */
      env.dicValue = new DicValue(task + '/VERSION_NUMBER', null,
        env.dns.url());

      try {
        const prom = waitForValue(env.dicValue, 'value', (val) => val,
          VersionNumber);

        const disNode = confNode.getDisNode(); /* spawn a Dis Node */
        expect(disNode).to.be.instanceOf(DisNode);

        await prom;

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('automatically reconnects on Dns Server disconnection',
    function(done) {
      const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
        port: "2505", retry: "500" } ];

      helper.load(DimConf, flow, async function() {
        const confNode = helper.getNode("conf");
        const dnsClient = confNode.getDnsClient();

        try {
          var prom = waitEvent(dnsClient, 'connect');
          dnsClient.connect();
          await prom;
          expect(confNode._connected).to.be.true();

          prom = waitEvent(dnsClient, 'disconnect');
          env.dns.close();
          await prom;
          expect(confNode._connected).to.be.false();

          prom = waitEvent(dnsClient, 'connect', 1, 1500);
          env.dns.listen();
          await prom;
          expect(confNode._connected).to.be.true();

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

  it('raises an error if the Dns Configuration is wrong', function(done) {
    const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
      port: "1234" } ];

    helper.load(DimConf, flow, function() {
      const confNode = helper.getNode("conf");
      confNode.getDisNode();

      confNode.once('call:error', function(call) {
        try {
          expect(call.firstArg.toString())
          .to.be.deep.equal('Error: failed to connect');

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });
  });

  it('releases resources once no longer needed', function(done) {
    const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
      port: "2505", retry: "1000" } ];

    helper.load(DimConf, flow, async function() {
      const confNode = helper.getNode("conf");

      try {
        expect(confNode._dnsClient).to.be.null();
        expect(confNode._disNode).to.be.null();

        confNode.getDisNode();
        await waitEvent(confNode._dnsClient, 'connect');
        confNode.ref();

        expect(confNode._dnsClient).to.be.instanceOf(DnsClient);
        expect(confNode._disNode).to.be.instanceOf(DisNode);

        const prom = waitEvent(confNode._dnsClient, 'close');
        confNode.unref();
        await prom;

        expect(confNode._dnsClient).to.be.null();
        expect(confNode._disNode).to.be.null();

        done();
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('raises a warning upon the DIS_DNS/KILL_SERVERS', function(done) {
    const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
      port: "2505", retry: "1000" } ];

    helper.load(DimConf, flow, async function() {
      const confNode = helper.getNode("conf");

      confNode.once('call:warn', function(call) {
        try {
          expect(call.args.toString()).to.be.deep
          .equal('dimConf.error.exit-command');
          done();
        }
        catch (err) {
          done(err);
        }
      });

      try {
        const prom = waitEvent(confNode.getDnsClient(), 'connect');
        confNode.getDisNode();
        await prom;

        DicCmd.invoke('DIS_DNS/KILL_SERVERS', null, env.dns.url());
      }
      catch (err) {
        done(err);
      }
    });
  });

  it('can retrieve the service list from a DNS server', function(done) {
    const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
      port: "2505", retry: "1000" } ];

    helper.load(DimConf, flow, function() {
      const confNode = helper.getNode("conf");

      /* request to a wrong DNS server */
      helper.request().get('/service-list/localhost:1234')
      .expect(500)
      .catch(done);


      helper.request()
      .get('/service-list/' + confNode._host + ':' + confNode._port)
      .expect(200)
      .then((res) => {
        expect(res.body).to.haveOwnProperty('list');
        expect(res.body.list).to.have.deep.members(
          /* DNS Built-in Services */
          [
            { name: 'DIS_DNS/SERVICE_INFO', definition: 'C,C', type: 'RPC' },
            { name: 'DIS_DNS/KILL_SERVERS', definition: 'I', type: 'CMD' },
            { name: 'DIS_DNS/SERVER_LIST', definition: 'C', type: 'SRV' }
          ]
        );
        done();
      })
      .catch(done);
    });
  });

  describe('can serve request for DIS built-in services:', function() {

    it('VERSION_NUMBER', function(done) {
      const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
        port: "2505" } ];

      helper.load(DimConf, flow, async function() {
        const confNode = helper.getNode("conf");
        env.dicValue = new DicValue(task + '/VERSION_NUMBER', null,
          env.dns.url());

        try {
          const prom = waitForValue(env.dicValue, 'value', (val) => val,
            VersionNumber);

          confNode.getDisNode();
          await prom;
          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('SERVICE_LIST', function(done) {
      const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
        port: "2505" } ];

      helper.load(DimConf, flow, async function() {
        const confNode = helper.getNode("conf");
        env.dicValue = new DicValue(task + '/SERVICE_LIST', null,
          env.dns.url());

        try {
          const prom = waitForValue(env.dicValue, 'value', (val) => val,
            task + "/SERVICE_LIST|C|\n" +
            task + "/VERSION_NUMBER|L:1|\n" +
            task + "/CLIENT_LIST|C|\n" +
            task + "/SET_EXIT_HANDLER|L:1|CMD\n" +
            task + "/EXIT|L:1|CMD");

          confNode.getDisNode();
          await prom;
          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('CLIENT_LIST', function(done) {
      const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
        port: "2505" } ];

      helper.load(DimConf, flow, async function() {
        const confNode = helper.getNode("conf");
        env.dicValue = new DicValue(task + '/CLIENT_LIST', null,
          env.dns.url());

        try {
          const prom = waitForValue(env.dicValue, 'value', (val) => val,
            task + '@' + node);

          confNode.getDisNode();
          await prom;
          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('SET_EXIT_HANDLER', function(done) {
      const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
        port: "2505" } ];

      helper.load(DimConf, flow, async function() {
        const confNode = helper.getNode("conf");
        const dnsClient = confNode.getDnsClient();

        try {
          let prom = waitEvent(dnsClient, 'connect');
          const disNode = confNode.getDisNode();
          await prom;

          DicCmd.invoke(task + '/SET_EXIT_HANDLER', 3, env.dns.url());

          prom = waitForValue(disNode, 'exit', (code) => code, 3);
          dnsClient.close(); /* DisNode disconnection */
          await prom;

          done();
        }
        catch (err) {
          done(err);
        }
      });
    });

    it('EXIT', function(done) {
      const flow = [ { id: "conf", type: "dim-conf", host: "localhost",
        port: "2505" } ];

      helper.load(DimConf, flow, async function() {
        const confNode = helper.getNode("conf");

        try {
          let prom = waitEvent(confNode.getDnsClient(), 'connect');
          const disNode = confNode.getDisNode();
          await prom;

          prom = waitForValue(disNode, 'exit', (code) => code, 3);

          DicCmd.invoke(task + '/EXIT', 3, env.dns.url());
          await prom;
          done();
        }
        catch (err) {
          done(err);
        }
      });
    });
  });
});
